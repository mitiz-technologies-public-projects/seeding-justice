import React, { useEffect, useState } from "react";
import {
  Day,
  Inject,
  Month,
  Resize,
  ScheduleComponent,
  ViewDirective,
  ViewsDirective,
  Week,
} from "@syncfusion/ej2-react-schedule";
import moment from "moment";
import dashboard from "../../Services/dashboard";
import LoadingOverlay from "react-loading-overlay";
import { Spinner } from "reactstrap";

const Scheduler = () => {
  const [currDate, setCurrDate] = useState(new Date(moment()));
  const [dmfData, setDmfData] = useState([]);
  const [currView, setCurrView] = useState("Month");
  const [eventLoader, setEventLoader] = useState(false);

  const calendarData = () => {
    setEventLoader(true);
    dashboard
      .submission({
        date: moment(currDate).format("YYYY-MM-DD"),
        view: currView,
      })
      .then((res) => {
        if (res?.data?.status) {
          let cdata = [];
          res?.data?.data.map((data) => {
            const { id, subject, year, month, day } = data;
            cdata.push({
              Id: id,
              Subject: subject,
              StartTime: new Date(
                parseInt(year),
                parseInt(month - 1),
                parseInt(day),
                9,
                10
              ),
              EndTime: new Date(
                parseInt(year),
                parseInt(month - 1),
                parseInt(day),
                9,
                30
              ),
            });
          });
          setDmfData(cdata);
          setEventLoader(false);
        }
      })
      .catch(function (error) {
        console.log("error", error);
        setEventLoader(false);
      });
  };

  // let cdata = [];
  // dmfData.map((data)=>{
  //   const {id, subject, year, month, day} = data
  //   cdata.push(
  //     {
  //       Id : id,
  //       Subject : subject,
  //       StartTime : new Date(parseInt(year),parseInt(month-1),parseInt(day),9,10),
  //       EndTime : new Date(parseInt(year),parseInt(month-1),parseInt(day),9,30)
  //     }
  //   );
  // });

  useEffect(() => {
    calendarData();
  }, [currDate, currView]);

  return (
    <>
      <LoadingOverlay
        active={eventLoader}
        spinner={<Spinner style={{color: "#00344B"}} />}
        fadeSpeed={200}
      >
        <ScheduleComponent
          currentView="Month"
          eventSettings={{ dataSource: dmfData }}
          readonly={true}
          rowAutoHeight={true}
          navigating={(e) => {
            setCurrDate(e.currentDate);
            setCurrView(e.currentView ? e.currentView : currView);
          }}
        >
          <ViewsDirective>
            <ViewDirective option="Day" />
            <ViewDirective option="Week" />
            <ViewDirective option="Month" />
          </ViewsDirective>
          <Inject services={[Month, Day, Week, Resize]} />
        </ScheduleComponent>
      </LoadingOverlay>
    </>
  );
};

export default Scheduler;
