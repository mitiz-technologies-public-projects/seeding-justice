import React, { useState } from "react";
import moment from "moment";
import { MdModeEdit } from "react-icons/md";
import { Link } from "react-router-dom";
import PrevettedDetailsBody from "./PrevettedDetailsBody";
import { FaEye } from "react-icons/fa";
import { BsCircle, BsXCircleFill } from "react-icons/bs";
import { Badge } from "reactstrap";
import { AiFillCheckCircle } from "react-icons/ai";
import {
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Dropdown,
} from "reactstrap";
import { FaEllipsisV } from "react-icons/fa";
import InvitationModal from "./InvitationModal";

const User = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [openContract, setOpenContract] = useState(false);
  const [opendetail, setOpenDetail] = useState(false);
  const [contractId, setContractId] = useState("");
  const [detailId, setDetailId] = useState("");

  const prevettedDetailsModal = (detail_id) => {
    setOpenDetail(true);
    setDetailId(detail_id);
  };

  const SendContract = (contractId) => {
    setOpenContract(true);
    setContractId(contractId);
  };

  const sjgrantee = props.sjgrantee;
  return (
    <>
      <tr key={sjgrantee.id}>
        <td>{props.index + 1}</td>
        <td>
          <Link
            to={""}
            onClick={() => prevettedDetailsModal(sjgrantee.id)}
            className="text-decoration-none"
          >
            {sjgrantee.org_name}
          </Link>
        </td>
        <td>{sjgrantee.ein_number}</td>
        <td>{moment(sjgrantee.created_at).format("D MMM,  YYYY")}</td>
        <td>
          {sjgrantee.status == 0 ? (
            <Badge pill color="danger">
              Inactive
            </Badge>
          ) : (
            <Badge pill color="success">
              Active
            </Badge>
          )}
        </td>

        <td className="text-center">
          <Dropdown
            toggle={() => {
              setDropdownOpen(!dropdownOpen);
            }}
            isOpen={dropdownOpen}
            color="primary"
            className="modal_Dropdown"
          >
            <DropdownToggle className="dropdown_btn">
              <FaEllipsisV />
            </DropdownToggle>
            <DropdownMenu container="body" right>
              <span onClick={() => prevettedDetailsModal(sjgrantee.id)}>
                <DropdownItem>
                  <FaEye
                    className="me-3 details-icon"
                    size={20}
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Details
                </DropdownItem>
              </span>
              <span onClick={() => props.openEditModal(sjgrantee.id)}>
                <DropdownItem>
                  <MdModeEdit
                    className="me-3 edit-icon"
                    size={20}
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Edit
                </DropdownItem>
              </span>
              <span onClick={() => props.deletePrevetted(sjgrantee.id)}>
                <DropdownItem>
                  <BsXCircleFill
                    className="me-3 remove-icon"
                    size={20}
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Delete
                </DropdownItem>
              </span>
            </DropdownMenu>
          </Dropdown>
        </td>
      </tr>
      {opendetail && (
        <PrevettedDetailsBody
          opendetail={opendetail}
          handleClose={() => {
            setOpenDetail();
            setDetailId(null);
          }}
          sjId={detailId}
          sjgrantee={sjgrantee}
        />
      )}

      {/* {openContract && (
        <InvitationModal
          openContract={openContract}
          handleClose={() => {
            setOpenContract();
            setContractId(null);
          }}
          contractId={contractId}
          // getPrevettedGrantees={props.getPrevettedGrantees}
        />
      )} */}
    </>
  );
};

export default User;
