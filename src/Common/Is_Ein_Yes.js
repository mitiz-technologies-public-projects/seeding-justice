import React, { useState, useEffect } from "react";
import { Row, Col, FormGroup, Label, Input, FormFeedback } from "reactstrap";
import CurrencyFormat from "react-currency-format";
import AsyncCreatableSelect from "react-select/async-creatable";
import { GrClose } from "react-icons/gr";
import userdashboard from "../Services/userdashboard";

const Is_Ein_Yes = (props) => {
  const [fields, setFields] = useState({});
  const [ein, setEin] = useState([]);
  const [selectInput, setSelectInput] = useState("");

  const handleInputChange = (inputValue, action) => {
    if (action.action !== "input-blur" && action.action !== "menu-close") {
      setSelectInput(inputValue);
      setFields({
        ...fields,
        ["ein_number_list"]: { label: inputValue, value: inputValue },
        ["ein_number"]: inputValue,
      });
    }
  };

  const { errors } = props;

  const handleChange = (e, field) => {
    setFields({ ...fields, [field]: e.target.value });
  };

  const handleEinChange = (value) => {
    setSelectInput("");
    setFields({
      ...fields,
      ["ein_number_list"]: value,
      ["ein_number"]: value?.value,
    });
  };

  const getEin = () => {
    userdashboard
      .org_list()
      .then((res) => {
        if (res?.data?.status) {
          if (res?.data?.data) {
            let fields = [];
            res.data.data.forEach((ein, index) => {
              fields[index] = {
                label: ein.org_name + " - " + ein.ein_number,

                value: ein.ein_number,
              };
            });
            setEin(fields);
          }
        }
      })
      .catch(function (error) {
        console.log("error", error);
      });
  };

  const formatOptionLabel = ({ value, label }) => {
    if(fields.ein_number){
      return (
        <div style={{ display: "flex" }}>
          <div>
            {value}
            </div>
        </div>
      );
    }else{
      return (
        <div style={{ display: "flex" }}>
          <div>
            {label}
            <br/>
            {value}
            </div>
        </div>
      );
    }
  }

  const loadOptions = (searchValue, callback) => {
    setTimeout(() => {
      const filteredOptions = ein.filter((option) =>
        option.value.toLowerCase().includes(searchValue.toLowerCase()) || option.label.toLowerCase().includes(searchValue.toLowerCase())
      );
      const menuEl = document.querySelector(".select__menu");
      if (filteredOptions.length >= 1 && menuEl) {
        menuEl.style.display = "block";
      } else {
        menuEl.style.display = "none";
      }
      callback(filteredOptions);
    }, 0);
  };

  useEffect(() => {
    props.updateFields(fields, props.keyValue);
  }, [fields]);
  useEffect(() => {
    getEin();
  }, []);
  return (
    <div>
      <div id="inputGroup" className="inputFields my-4">
        {props.keyValue > 0 && (
          <span className="d-flex justify-content-end" title="Remove Fields">
            <GrClose
              onClick={() => props.removeYesBlock(props.keyValue)}
              style={{ cursor: "pointer" }}
            />
          </span>
        )}
        <Row>
          <Col md={6}>
            <FormGroup>
              <Label>
                EIN Number (To search EIN number{" "}
                <a
                  href="https://www.guidestar.org/search/"
                  target="_blank"
                  rel="noreferrer"
                  style={{color:"#E26A38"}}
                >
                  here
                </a>
                )
              </Label>
              <AsyncCreatableSelect
                loadOptions={loadOptions}
                isClearable={true}
                defaultOptions={ein}
                id="ein_number_list"
                name="ein_number_list"
                placeholder={<div>Write EIN number</div>}
                formatCreateLabel={() => undefined}
                value={
                  fields["ein_number_list"] ? fields["ein_number_list"] : []
                }
                classNamePrefix="select"
                inputValue={selectInput}
                onInputChange={handleInputChange}
                onChange={handleEinChange}
              />
              {errors[`ein_yes.${props.keyValue}.ein_number`] && (
                <small className="fa-1x text-danger">
                  {errors[`ein_yes.${props.keyValue}.ein_number`]}
                </small>
              )}
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="recognition_id">
                Would you like Donor Recognition or Anonymity?
              </Label>
              <Input
                type="select"
                name="recognition_id"
                value={fields["recognition_id"] ? fields["recognition_id"] : ""}
                onChange={(event) => handleChange(event, "recognition_id")}
                invalid={
                  errors[`ein_yes.${props.keyValue}.recognition_id`]
                    ? true
                    : false
                }
              >
                <option value="">
                  {props.nominationDropdowns.length === 0
                    ? "Loading..."
                    : "-Select-"}
                </option>
                {props.nominationDropdowns.length > 0 &&
                  props.nominationDropdowns.map((dropdowns, i) => (
                    <option value={dropdowns.id} key={`key-dropdowns-${i}`}>
                      {dropdowns.category_name}
                    </option>
                  ))}
              </Input>
              <FormFeedback>
                {errors[`ein_yes.${props.keyValue}.recognition_id`]}
              </FormFeedback>
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup className="initial_fund">
              <Label for="grant_amount">Grant Amount</Label>
              <CurrencyFormat
                name="grant_amount"
                placeholder="$0.00"
                inputmode="numeric"
                className="form-control"
                value={fields["grant_amount"] ? fields["grant_amount"] : ""}
                thousandSeparator={true}
                invalid={
                  errors[`ein_yes.${props.keyValue}.grant_amount`]
                    ? true
                    : false
                }
                prefix={"$"}
                onChange={(event) => handleChange(event, "grant_amount")}
              />
              {errors[`ein_yes.${props.keyValue}.grant_amount`] && (
                <small className="fa-1x text-danger">
                  {errors[`ein_yes.${props.keyValue}.grant_amount`]}
                </small>
              )}
            </FormGroup>
          </Col>
        </Row>
        <Col md={12}>
          <FormGroup>
            <Label for="recognition_id">
              Would you like Donor Recognition or Anonymity?
            </Label>
            <Input
              type="select"
              name="recognition_id"
              value={fields["recognition_id"] ? fields["recognition_id"] : ""}
              onChange={(event) => handleChange(event, "recognition_id")}
              invalid={
                errors[`ein_yes.${props.keyValue}.recognition_id`]
                  ? true
                  : false
              }
            >
              <option value="">
                {props.nominationDropdowns.length === 0
                  ? "Loading..."
                  : "-Select-"}
              </option>
              {props.nominationDropdowns.length > 0 &&
                props.nominationDropdowns.map((dropdowns, i) => (
                  <option value={dropdowns.id} key={`key-dropdowns-${i}`}>
                    {dropdowns.category_name}
                  </option>
                ))}
            </Input>
            <FormFeedback>
              {errors[`ein_yes.${props.keyValue}.recognition_id`]}
            </FormFeedback>
          </FormGroup>
        </Col>
      </div>
    </div>
  );
};

export default Is_Ein_Yes;
