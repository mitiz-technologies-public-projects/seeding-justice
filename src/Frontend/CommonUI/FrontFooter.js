import React from "react";
import { Container, Row, Col } from "reactstrap";
import { BsTwitter } from "react-icons/bs";
import { Link } from "react-router-dom";
import { FaFacebookF, FaInstagram, FaMedium } from "react-icons/fa";

function FrontFooter(props) {
  return (
    <>
      <footer className="pt-5 frontend-footer">
        <Container>
          <Row className="footer_item">
            <Col md="3">
              <ul>
                <li>
                  <Link
                    to={"https://www.seedingjustice.org/annual-reports/"}
                    target="_blank"
                  >
                    Annual Reports
                  </Link>
                </li>
                <li>
                  <Link
                    to={"https://www.seedingjustice.org/news-events/"}
                    target="_blank"
                  >
                    News & Events
                  </Link>
                </li>
                <li>
                  <Link
                    to={"https://www.seedingjustice.org/privacy-policy/"}
                    target="_blank"
                  >
                    Privacy Policy
                  </Link>
                </li>
                <li>
                  <Link to={"/faqs"}>FAQs</Link>
                </li>
              </ul>
            </Col>
            <Col md="3">
              <h6>About Us </h6>
              <ul className="footer_details">
                <li>Seeding Justice (formerly MRG</li>
                <li> Foundation) is the region’s leading</li>
                <li> funder of social change organizing.</li>
                <li> We are a 501c3 organization.</li>
                <li>Our EIN is 93-0691187.</li>
              </ul>
            </Col>
            <Col md="4">
              <h6>Contact Us</h6>
              <ul className="footer_details">
                <li>P.O. Box 12489</li>
                <li>Portland, Oregon 97212</li>
                <li>Call: 503-289-1517</li>
                <li>
                  <Link to="mailto:nfo@seedingjustice.org">
                    Email: info@seedingjustice.org
                  </Link>
                </li>
              </ul>
            </Col>
            <Col md="2">
              <Link
                to="/"
                className="nav-logo"
                style={{ textDecoration: "none" }}
              >
                <img
                  src="/dev/assets/seeding-justice-logo2.png"
                  alt="seedingjustice"
                />
              </Link>
              <ul className="social_media">
                <li>
                  <Link
                    to={"https://www.facebook.com/seedingjustice"}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <FaFacebookF />
                  </Link>
                </li>
                <li>
                  <Link
                    to={"https://twitter.com/seedingjustice"}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <BsTwitter />
                  </Link>
                </li>
                <li>
                  <Link
                    to={"https://www.instagram.com/seedingjustice/"}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <FaInstagram />
                  </Link>
                </li>
                <li>
                  <Link
                    to={"https://seedingjustice.medium.com/"}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <FaMedium />
                  </Link>
                </li>
              </ul>
            </Col>
          </Row>
        </Container>
        <div className="footer-bottom mt-4">
          <Container>
            <Row>
              <Col sm="12 text-center pt-4 pb-3">
                <p style={{ fontSize: "12px" }}>
                  {/* Copyright © Seeding Justice {moment().format("YYYY")} */}
                  Copyright 2021 Seeding Justice
                </p>
              </Col>
            </Row>
          </Container>
        </div>
      </footer>
    </>
  );
}

export default FrontFooter;
