import React, { useState, useEffect } from "react";
import { Container, Button } from "reactstrap";
import { BsFillGrid3X3GapFill } from "react-icons/bs";
import {
  DropdownMenu,
  DropdownItem,
  ButtonDropdown,
  DropdownToggle,
} from "reactstrap";
import { useSelector, useDispatch } from "react-redux";
import { Link, useNavigate, NavLink, useLocation } from "react-router-dom";
import user from "../../Services/user";
import { BiLogOut } from "react-icons/bi";
import logo from "./logo.png";

function FrontHeader() {
  const token = useSelector((state) => state.token);
  const first_name = useSelector((state) => state.first_name);
  const last_name = useSelector((state) => state.last_name);
  const prevetted_grantee = localStorage.getItem("prevetted_grantee_id");

  const [dropdownOpen, setDropdownOpen] = React.useState(false);
  const [click, setClick] = React.useState(false);
  const [scroll, setScroll] = useState(false);
  const handleClick = () => setClick(!click);
  const Close = () => setClick(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 10);
    });
  }, []);

  const handleLogout = () => {
    user
      .logout()
      .then((res) => {
        if (res.data.status) {
          localStorage.clear();
          dispatch({ type: "logout" });
          navigate("/");
        }
      })
      .catch((error) => {
        localStorage.clear();
        dispatch({ type: "logout" });
        navigate("/");
      });
  };

  return (
    <div>
      <div className={click ? "main-container" : ""} onClick={() => Close()} />
      <nav
        className={scroll ? "front-navbar fixed-navbar" : "front-navbar"}
        onClick={(e) => e.stopPropagation()}
      >
        <Container>
          <div className="nav-container">
            <Link
              to="/"
              className="nav-logo"
              style={{ textDecoration: "none" }}
            >
              {prevetted_grantee ? (
                <img src={logo} alt="seedingjustice" />
              ) : (
                <img src="/dev/assets/logo.png" alt="seedingjustice" />
              )}
            </Link>
            <div className={click ? "nav-menu active" : "nav-menu"}>
              <ul>
                {/* <li className="nav-item">
                  <NavLink
                    to="/"
                    activeClassName="active"
                    className="nav-links"
                    onClick={click ? handleClick : null}
                  >
                    Home
                  </NavLink>
                </li> */}

                {token ? (
                  prevetted_grantee !== "null" ? (
                    <li className="nav-item">
                      <NavLink
                        to="/prevetted/dmf-form"
                        activeClassName="active"
                        className="nav-links"
                        onClick={click ? handleClick : null}
                      >
                        Open a DMF
                      </NavLink>
                    </li>
                  ) : (
                    <li className="nav-item">
                      <NavLink
                        to="/dmf-form"
                        activeClassName="active"
                        className="nav-links"
                        onClick={click ? handleClick : null}
                      >
                        Open a DMF
                      </NavLink>
                    </li>
                  )
                ) : (
                  ""
                )}

                {token ? (
                  prevetted_grantee !== "null" ? (
                    <li className="nav-item">
                      <NavLink
                        to="/prevetted/dashboard"
                        activeClassName="active"
                        className="nav-links"
                        onClick={click ? handleClick : null}
                      >
                        Dashboard
                      </NavLink>
                    </li>
                  ) : (
                    <li className="nav-item">
                      <NavLink
                        to="/giving-history"
                        activeClassName="active"
                        className="nav-links"
                        onClick={click ? handleClick : null}
                      >
                        Dashboard
                      </NavLink>
                    </li>
                  )
                ) : (
                  ""
                )}

                {!token ? (
                  <li className="nav-item">
                    <Link to="login">
                      <Button
                        color="success"
                        size="md"
                        onClick={click ? handleClick : null}
                      >
                        <BiLogOut className="me-2 mb-0" />
                        Sign In
                      </Button>
                    </Link>
                  </li>
                ) : (
                  ""
                )}

                {token ? (
                  <ButtonDropdown
                    toggle={() => {
                      setDropdownOpen(!dropdownOpen);
                    }}
                    isOpen={dropdownOpen}
                  >
                    <DropdownToggle className="toggle">
                      {/* <img src="./assets/pexels-photo-220453 2.png" style={{ height: '50px', borderRadius: '50%'}} /> */}
                      <div className="profileImage">
                        <span>{`${first_name.charAt(0)}${last_name.charAt(
                          0
                        )} `}</span>
                      </div>
                    </DropdownToggle>
                    <DropdownMenu end>
                      {prevetted_grantee !== "null" ? (
                        ""
                      ) : (
                        <Link
                          to="/edit-profile"
                          className="text-decoration-none text-dark"
                        >
                          <DropdownItem>My account</DropdownItem>
                        </Link>
                      )}

                      <Link
                        to="/change-password"
                        className="text-decoration-none text-dark"
                      >
                        <DropdownItem>Change password</DropdownItem>
                      </Link>
                      <DropdownItem onClick={handleLogout}>Logout</DropdownItem>
                    </DropdownMenu>
                  </ButtonDropdown>
                ) : (
                  ""
                )}
              </ul>
            </div>
            <div className="nav-icon" onClick={handleClick}>
              <BsFillGrid3X3GapFill />
            </div>
          </div>
        </Container>
      </nav>
    </div>
  );
}

export default FrontHeader;
