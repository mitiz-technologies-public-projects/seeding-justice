import React, { Fragment, useEffect, useState } from "react";
import LoadingOverlay from "react-loading-overlay";
import Select from "react-select";
import { Card, CardHeader, Col, Container, Row, Spinner } from "reactstrap";
import prevettedsj from "../../Services/prevettedGrantees";
import userdashboard from "../../Services/userdashboard";
import PrevettedData from "./PrevettedData";
import Sidebar from "../Sidebar";

const Index = () => {
  const [granteesData, setGranteesData] = useState({});
  const [loader, setLoader] = useState(false);
  const [isLoader, setIsLoader] = useState(false);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState([]);
  const [categories, setCategories] = useState({});

  LoadingOverlay.propTypes = undefined;
  
  const handleAreasChange = (value) => {
    categories["categories"] = value;
    setIsLoader(true);
    setCategories(categories);
    let params = value.value;
    userdashboard
      .listsjgrantees({ category_id: params })
      .then((res) => {
        if (res?.data?.status) {
          setGranteesData(res?.data?.data?.data);
          setLoader(false);
          setIsLoader(false);
        }
      })
      .catch(function (error) {
        setLoader(false);
        console.log("error", error);
      });
  };

  const getGranteelist = () => {
    setLoader(true);
    let params = {
      page: page,
    };
    userdashboard
      .listsjgrantees(params)
      .then((res) => {
        if (res?.data?.status) {
          setGranteesData(res?.data?.data?.data);
          setPage(res?.data?.data.current_page);
          setTotalPages(res?.data?.data.total / res?.data?.data.per_page);
          setLoader(false);
        }
      })
      .catch(function (error) {
        setLoader(false);
        console.log("error", error);
      });
  };

  const getCategories = () => {
    setLoader(true);
    prevettedsj
      .prevttedcategorylist()
      .then((res) => {
        if (res.data.status) {
          let categories = [];
          res.data.data.forEach((category, index) => {
            categories[index] = {
              label: category.category_name,
              value: category.id,
            };
          });
          setCategories(categories);
        }
        setLoader(false);
      })
      .catch(function (error) {
        console.log("error", error);
        setLoader(true);
      });
  };

  useEffect(() => {
    getGranteelist(page);
    getCategories();
  }, [page]);

  return (
    <>
      <Container className="px-5 pt-5">
        <Row>
          <Col sm={3}>
            <Sidebar />
          </Col>
          <Col sm={9}>
            <Card
              className="shadow support_aside mb-5"
              style={{ minHeight: "350px" }}
            >
              <CardHeader className="py-3">
                <h4 className="mb-0">Recommended Grantees</h4>
                <hr />
                <Select
                  id="categories"
                  name="categories"
                  placeholder={<div>Select Grantee Areas</div>}
                  value={
                    categories["categories"] ? categories["categories"] : []
                  }
                  options={categories}
                  classNamePrefix="select"
                  onChange={handleAreasChange}
                />
              </CardHeader>
              <Fragment>
                <Fragment>
                  {loader ? (
                    <div
                      className="d-flex align-items-center justify-content-center"
                      style={{ height: "250px" }}
                    >
                      <Spinner
                        style={{
                          height: "60px",
                          width: "60px",
                        }}
                      />
                    </div>
                  ) : (
                    <Row className="p-3 PrevettedData">
                      {granteesData?.length > 0 ? (
                        <LoadingOverlay
                          active={isLoader}
                          spinner={
                            <Spinner
                            style={{ height: 50, width: 50, color: "#00344B" }}
                            />
                          }
                          fadeSpeed={100}
                          classNamePrefix="mitiz"
                        >
                          <div>
                            {granteesData?.map((sjgrantee, index) => (
                              <PrevettedData
                                key={index}
                                sjgrantee={sjgrantee}
                              />
                            ))}
                          </div>
                        </LoadingOverlay>
                      ) : (
                        !loader && (
                          <div>
                            <div key={0} colSpan="6" className="text-center">
                              <p className="text-center">Record not found.</p>
                            </div>
                          </div>
                        )
                      )}
                    </Row>
                  )}
                </Fragment>
              </Fragment>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};
export default Index;
