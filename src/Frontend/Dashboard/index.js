import React from "react";
import { Row, Container } from "reactstrap";
import Sidebar from "./Sidebar.js";

const Dashboard = () => {
  return (
    <Container className="px-5 pt-5">
      <Row>
        <Sidebar />
      </Row>
    </Container>
  );
};
export default Dashboard;
