import React from "react";
import { Container, Row, Col, Button } from "reactstrap";
import { BsArrowRightShort } from "react-icons/bs";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

function Home(props) {
  return (
    <>
      <Helmet>
        <title>Home : seeding-Justice</title>
      </Helmet>
      <div className="login_section">
        <Container className="px-5">
          <Row className="justify-content-between align-items-center">
            <Col md={4} className="mb-4">
              <h2 className="text-dark">Our Community Garden</h2>
              <p className="text-dark" style={{ textAlign: "justify" }}>
                Welcome to Our Community Garden, the secure portal to manage
                your Seeding Justice Donor-in-Movement Fund. Please note:
                password and security questions are case sensitive. 
              </p>
              <p className="text-dark">
                If you have not set up a Community Garden account yet, please
                contact us at  <a href="mailto:XYZ@seedingjustice.com">XYZ@seedingjustice.com. </a> If you are interested in
                opening a Donor-in-Movement Fund, please fill out this <Link to="/dmf-form"> form </Link>
                with the new donor advised fund account.
              </p>
            </Col>

            <Col md={6} className="text-center">
              <img
                src="./assets/windows.svg"
                style={{ width: "100%" }}
                alt="seedingJustice"
              />
            </Col>
          </Row>
        </Container>
      </div>
      <div className="svg-border-rounded text-white banner-arc">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 144.54 17.34"
          preserveAspectRatio="none"
          fill="currentColor"
        >
          <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
        </svg>
      </div>
    </>
  );
}

export default Home;
