import React, { useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";

import user from "../../../Services/user";
import { useDispatch, useSelector } from "react-redux";
import { Outlet } from "react-router-dom";
import FrontHeader from "../../CommonUI/FrontHeader";
import FrontFooter from "../../CommonUI/FrontFooter";

function PrevettedLayout() {
  const isAdmin = useSelector((state) => state.isAdmin);
  const first_name = useSelector((state) => state.first_name);
  const [dropdownOpen, setOpen] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [activeMenu, setActiveMenu] = useState();
  const location = useLocation();

  useEffect(() => {
    let route_name = location.pathname.split("/");
    route_name = route_name[route_name.length - 1];
    setActiveMenu(route_name);
  }, [location]);

  const handleLogout = () => {
    user
      .logout()
      .then((res) => {
        if (res.data.status) {
          localStorage.clear();
          dispatch({ type: "logout" });
          navigate("/");
        }
      })
      .catch((error) => {
        localStorage.clear();
        dispatch({ type: "logout" });
        navigate("/");
      });
  };

  const handleClick = (event) => {
    setIsActive((current) => !current);
  };

  //   if (parseInt(isAdmin) !== 1) {
  //     return <Navigate to="/" />;
  //   } else {
  return (
    <>
      <FrontHeader />
      <Outlet />
      <FrontFooter />
    </>
  );
}
// }
export default PrevettedLayout;
