import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Collapse,
  Container,
  Row,
  Spinner,
} from "reactstrap";
import Sidebar from "../Sidebar";
import faq from "../../../Services/faq";
import { AiOutlineDown } from "react-icons/ai";
import LoadingOverlay from "react-loading-overlay";

const Index = () => {
    const [loader,setLoader] = useState(false)


  return (
    <div className="userAdminPanel">
      <Row className="customised-row">
        <Col md={3}>
          <Sidebar />
        </Col>
        <Col md={9}>
          <div className="mt-4 mb-5 userAdminArticle">
            <div className="py-3 userAdminArticleHeading">
              <h4 className="mb-0">Frequently Asked Questions</h4>
            </div>
            <LoadingOverlay
              active={loader}
              spinner={
                <Spinner style={{ height: 60, width: 60, color: "#00344B" }} />
              }
              fadeSpeed={100}
              classNamePrefix="mitiz"
            >
              <CardBody className="recommend-grants-form px-2">
                <div style={{ margin: "3rem 0" }}>
                  <div className="custom_accordian">
                    
                  </div>
                </div>
              </CardBody>
            </LoadingOverlay>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Index;
