import React, { useState, useEffect } from "react";
import {
  Container,
  ButtonDropdown,
  Navbar,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavItem,
  Nav,
} from "reactstrap";
import {
  Link,
  Navigate,
  Outlet,
  useNavigate,
  useLocation,
} from "react-router-dom";
import { BsJustifyRight, BsX } from "react-icons/bs";
import user from "../Services/user";
import { useDispatch, useSelector } from "react-redux";
import { AiOutlineDown } from "react-icons/ai";

function AdminLayout() {
  const isAdmin = useSelector((state) => state.isAdmin);
  const first_name = useSelector((state) => state.first_name);
  const [dropdownOpen, setOpen] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [activeMenu, setActiveMenu] = useState();
  const location = useLocation();

  useEffect(() => {
    let route_name = location.pathname.split("/");
    route_name = route_name[route_name.length - 1];
    setActiveMenu(route_name);
  }, [location]);

  const handleLogout = () => {
    user
      .logout()
      .then((res) => {
        if (res.data.status) {
          localStorage.clear();
          dispatch({ type: "logout" });
          navigate("/");
        }
      })
      .catch((error) => {
        localStorage.clear();
        dispatch({ type: "logout" });
        navigate("/");
      });
  };

  const handleClick = (event) => {
    setIsActive((current) => !current);
  };

  if (parseInt(isAdmin) !== 1) {
    return <Navigate to="/giving-history" />;
  } else {
    return (
      <>
        <div className="admin">
          <aside
            className={
              isActive
                ? "sidenav admin-aside-modification mobile-sideView overflow-hidden"
                : "sidenav admin-aside-modification overflow-hidden"
            }
          >
            <div className="sidenav-header">
              <div className="brand-logo">
                <Link to="/admin/dashboard">
                  <img alt="Logo" src="/dev/assets/logo.png" />
                </Link>
                <BsX className="sidebar-closing" onClick={handleClick} />
              </div>
            </div>
            <div navbar className="custom-scroll h-100">
              <div className="shadow-none support_aside mb-5">
                <Nav tabs fill className="admin-nav">
                  <NavItem>
                    <Link
                      to="/admin/dashboard"
                      className={
                        activeMenu === "dashboard"
                          ? "active nav-link nav-font"
                          : "nav-link nav-font"
                      }
                      onClick={() => {
                        setActiveMenu("dashboard");
                      }}
                    >
                      Dashboard
                    </Link>
                  </NavItem>
                  <NavItem>
                    <Link
                      to="/admin/dmf-submission"
                      className={
                        activeMenu === "dmf-submission"
                          ? "active nav-link nav-font"
                          : "nav-link nav-font"
                      }
                      onClick={() => {
                        setActiveMenu("dmf-submission");
                      }}
                    >
                      DMF
                      <br />
                      Submissions
                    </Link>
                  </NavItem>
                  <NavItem>
                    <Link
                      to="/admin/users"
                      className={
                        activeMenu === "users"
                          ? "active nav-link nav-font"
                          : "nav-link nav-font"
                      }
                      onClick={() => {
                        setActiveMenu("users");
                      }}
                    >
                      Donors
                    </Link>
                  </NavItem>
                  <NavItem>
                    <Link
                      to="/admin/nominations"
                      className={
                        activeMenu === "nominations"
                          ? "active nav-link nav-font"
                          : "nav-link nav-font"
                      }
                      onClick={() => {
                        setActiveMenu("nominations");
                      }}
                    >
                      Grant
                      <br />
                      Nominations
                    </Link>
                  </NavItem>
                  <NavItem>
                    <Link
                      to="/admin/grantees"
                      className={
                        activeMenu === "grantees"
                          ? "active nav-link nav-font"
                          : "nav-link nav-font"
                      }
                      onClick={() => {
                        setActiveMenu("grantees");
                      }}
                    >
                      Grantees
                    </Link>
                  </NavItem>
                  <NavItem>
                    <Link
                      to="/admin/prevetted-sjgrantees"
                      className={
                        activeMenu === "prevetted-sjgrantees"
                          ? "active nav-link nav-font"
                          : "nav-link nav-font"
                      }
                      onClick={() => {
                        setActiveMenu("prevetted-sjgrantees");
                      }}
                    >
                      Recommended
                      <br />
                      Grantees
                    </Link>
                  </NavItem>

                  <NavItem>
                    <Link
                      to="/admin/email-templates"
                      className={
                        location.pathname.startsWith("/admin/email-templates")
                          ? "active nav-link nav-font"
                          : "nav-link nav-font"
                      }
                      onClick={() => {
                        setActiveMenu("email-templates");
                      }}
                    >
                      Email
                      <br />
                      Templates
                    </Link>
                  </NavItem>

                  <NavItem>
                    <Link
                      to="/admin/settings/profile"
                      className={
                        location.pathname.startsWith("/admin/settings")
                          ? "active nav-link nav-font"
                          : "nav-link nav-font"
                      }
                      onClick={() => {
                        setActiveMenu("settings");
                      }}
                    >
                      Settings
                    </Link>
                  </NavItem>
                </Nav>
              </div>
            </div>
          </aside>
          <main className={isActive ? "blur-layout" : ""}>
            <Navbar className="admin-navbar m-0">
              <NavItem className="ms-auto">
                <ButtonDropdown
                  toggle={() => {
                    setOpen(!dropdownOpen);
                  }}
                  isOpen={dropdownOpen}
                  className="nav_admin_profile"
                >
                  {/* <FaUserCircle
                      className="user-profile"
                      style={{ fontSize: 35 }}
                    /> */}
                  <img
                    className="user-profile"
                    src="/dev/assets/user-profile.png"
                    style={{ height: "35px" }}
                  />
                  <span className="text-dark ms-3">{first_name}</span>
                  <DropdownToggle className="toggle">
                    <AiOutlineDown />
                  </DropdownToggle>
                  <DropdownMenu end>
                    <Link
                      style={{ height: "30px" }}
                      to="/admin/settings/profile"
                      className="text-decoration-none text-dark"
                    >
                      <DropdownItem> Settings</DropdownItem>
                    </Link>
                    <DropdownItem onClick={handleLogout}>Logout</DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </NavItem>
              <NavItem>
                <Link to="#">
                  <div className="sidenav-toggler-inner" onClick={handleClick}>
                    <BsJustifyRight />
                  </div>
                </Link>
              </NavItem>
            </Navbar>
            <Container className="pb-5" fluid>
              <div className="admin-body-layout">
                <Outlet />
                {/* <Row>
                  <Col>
                    <Card className="mb-4">
                      <CardBody>
                        <Outlet />
                      </CardBody>
                    </Card>
                  </Col>
                </Row> */}
              </div>
            </Container>
            <footer className="admin-footer">
              <div className="copyright">
                <span>Copyright &copy; Seeding Justice</span>
              </div>
            </footer>
          </main>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-sm-2"></div>
          </div>
        </div>
      </>
    );
  }
}
export default AdminLayout;
