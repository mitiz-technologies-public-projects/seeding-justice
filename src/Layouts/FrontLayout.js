import React from "react";
import {Navigate, Outlet} from "react-router-dom";
import FrontHeader from "../Frontend/CommonUI/FrontHeader";
import FrontFooter from "../Frontend/CommonUI/FrontFooter";
import { useSelector } from "react-redux";

function FrontLayout() {
  const isAdmin = useSelector((state) => state.isAdmin);

  
  if(parseInt(isAdmin) === 1){
    return <Navigate to="/admin/dashboard" />
  }
  return (
    <div>
      <FrontHeader />
      <Outlet />
      <FrontFooter />
    </div>

  );
}

export default FrontLayout;
