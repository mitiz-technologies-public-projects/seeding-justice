import http from "./http";

 const dashboard = {
    list: (param) => http.get("admin/dashboard", { params: param }),
    submission: (param) => http.get("admin/dashboard/calendar-data", { params: param }),
}

export default dashboard