import axios from "axios";
import store from "../Redux/store";
import { toast } from "react-toastify";

let apiUrl;
if (document.URL.includes("vikaslocalhost")) {
  apiUrl = "https://dmf.seedingjustice.org/dev/sj/public/api";
} else {  
  apiUrl = "https://dmf.seedingjustice.org/dev/sj/public/api";
}

const http = axios.create({
  baseURL: apiUrl,
  //timeout: 1000,
  headers: { "Content-Type": "application/json" },
});
http.interceptors.request.use(
  (config) => {
    if (store.getState().token) {
      config.headers.common["Authorization"] = `Bearer  ${
        store.getState().token
      }`;
    }
    return config;
  },
  (error) => {
    console.log(error);
    return Promise.reject(error);
  }
);

http.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status === 401) {
      localStorage.clear();
      window.location.href = "/";
    } else if (error.response.status === 404) {
      toast.error(error.message, {
        position: toast.POSITION.BOTTOM_RIGHT,
      });
    } else if (error.response.status === 500) {
      toast.error(error.message, {
        position: toast.POSITION.BOTTOM_RIGHT,
      });
    } else if (error.response.status === 403) {
      toast.error(error.message, {
        position: toast.POSITION.BOTTOM_RIGHT,
      });
    } else if (error.response.status === 415) {
      toast.error(
        error.message +
          " - Unsupported media type. Please upload image file only.",
        {
          position: toast.POSITION.BOTTOM_RIGHT,
        }
      );
    } else {
      return Promise.resolve(error.response);
    }
    return Promise.reject(error);
  }
);

export default http;