import http from "./http";

const report = {
  add: (data) => http.post("/admin/quarterly-report", data),
  list: (data) => http.get(`/admin/quarterly-report/${data.id}`, data),
  delete: (param) => http.delete(`/admin/quarterly-report/${param}`),
};

export default report;
